import React from 'react';
export default class Breweries extends React.Component {
	constructor(props) {
    super(props);

    this.state = {
    	br:null
    };
  }
  handleClick = (brewery) => {
   
  } 
 
 
  render(){
  	return (
        <div>
          <h1 style={{color:'#C0C0C0'}}>List breweries</h1>
          <ol style={{border:' 2px solid #C0C0C0'}} >
          	{this.props.breweries.map((brewerie,index)=>{
          		return <li key={index} onClick={() => this.props.handleChange(brewerie)}>
              <h5 >Name: {brewerie.name}</h5>
                <p style={{color:'#0000FF'}}> Type :{brewerie.brewery_type}</p>
              </li> 
          	})}             
            </ol>
              </div>
      ) 
  }
}

