import React from 'react';
import { Card, Icon, Image } from 'semantic-ui-react'

const Brewery = ({ brewery }) => {
      return (
       
      brewery && <div>
        <h1 style={{color:'#C0C0C0'}}>Brewery details</h1>
     <Card >
    
    <Card.Content>
      <Card.Header >Name : {brewery.name}</Card.Header>
     <Card.Meta  style={{color:'#0000FF'}}>Street :  {brewery.street}</Card.Meta>
     <Card.Meta style={{color:'#0000FF'}}>City : {brewery.city}</Card.Meta>
     <Card.Meta style={{color:'#0000FF'}}>State : {brewery.state}</Card.Meta>
     <Card.Meta style={{color:'#0000FF'}}>Code Postal : {brewery.postal_code}</Card.Meta>
     <Card.Meta style={{color:'#0000FF'}}>Country : {brewery.country}</Card.Meta>
     <Card.Meta style={{color:'#0000FF'}}>Longitude : {brewery.longitude}</Card.Meta>
     <Card.Meta style={{color:'#0000FF'}}>Latitude : {brewery.latitude}</Card.Meta>
     <Card.Meta style={{color:'#0000FF'}}>Phone : {brewery.phone}</Card.Meta>
   </Card.Content>
    
  </Card>
        </div>
       
        
      )
    };

export default Brewery

