import React from 'react';
import Breweries from '../../components/Breweries';
import Brewery from '../../components/Brewery';
import {
  Grid
} from 'semantic-ui-react'
export default class HomePage extends React.Component {

	constructor(props) {
    super(props);

    this.state = {
    	breweries: [],
    	isLoaded:true,
        brewery:null
    };
  } 
    handleChange=(brewery) => {
        this.setState({brewery: brewery});
    }

  componentDidMount() {
        fetch('https://api.openbrewerydb.org/breweries')
        .then(res => res.json())
        .then((data) => {
          this.setState({ breweries: data })
        })
        .catch(console.log)
      }
      

    	render() {
        return (
    <Grid columns={2} stackable>
      <Grid.Column>
        <Breweries handleChange={this.handleChange} breweries={this.state.breweries} />
      </Grid.Column>
      <Grid.Column>
        <Brewery   brewery={this.state.brewery}/>
      </Grid.Column>
 </Grid>
            
        )
      }
  }
 
             
         
         
    		 	
        
    	

